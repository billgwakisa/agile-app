import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
// import { map, catchError, tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';

// const endpoint = 'https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/';
// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json'
//   })
// };
// const token = localStorage.getItem('TOKEN');

@Injectable({
  providedIn: 'root'
})
export class RestService {
  // profileId = '';
  tenantId = '';

  constructor(private http: HttpClient) { }
  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
  getCustomerDetails(profileId, token): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get(`https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/899010/profiles/${profileId}/emailaddresses`, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}}).pipe(
      map(this.extractData));
  }

  getAddresses(profileId, token): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    console.log('The token ' + token);
    return this.http.get(`https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/899010/profiles/${profileId}/addresses`, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}}).pipe(
      map(this.extractData));
  }

  getPhones(profileId, token): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get(`https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/899010/profiles/${profileId}/phones`, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}}).pipe(
      map(this.extractData));
  }
  getEmailAddresses(profileId, token): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get(`https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/899010/profiles/${profileId}/emailaddresses`, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}}).pipe(
      map(this.extractData));
  }

  getContactPersons(profileId, token): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get(`https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/899010/profiles/${profileId}/contactpersons`, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}}).pipe(
      map(this.extractData));
  }

  sendBugReport(email, title, desc): boolean {
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    this.http.post('http://localhost:5001/api/bugReports',
      {
        'Email': email,
        'Title': title,
        'Description': desc
      }, {headers: {'Content-Type': 'application/json'}} )
      .subscribe(
        data => {
          console.log('Request is successful ', data);
          alert('Bug report sent!' + ' with ID: ' + data['id']);
          return true;
        },
        error => {
          console.log('Error', error);
          return false;
        }
      );
    return false;
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
