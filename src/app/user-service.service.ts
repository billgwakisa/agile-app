import {Injectable} from '@angular/core';

const TOKEN = 'TOKEN';
const PROFILE_ID = 'PROFILE_ID';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  setToken(token: string, profileId: string): void {
    localStorage.setItem(TOKEN, token);
    localStorage.setItem(PROFILE_ID, profileId);
  }

  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }
}
