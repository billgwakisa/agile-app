import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

export class HomePageComponent implements OnInit {

  title = 'Customer Details';
  customerDetails: any = [];
  addresses: any = [];
  phones: any = [];
  emailAddresses: any = [];
  contactPersons: any = [];
  bugReport: any = [];
  profileId = localStorage.getItem('PROFILE_ID');
  token = localStorage.getItem('TOKEN');
  constructor(private rest: RestService) { }

  ngOnInit(): void {
this.bugReport.response = 'No report to send';

    this.getCustomerDetails();
    this.getAddresses();
    this.getPhones();
    this.getEmailAddresses();
    this.getContactPersons();
  }

  setClientDetails(id) {
    this.getCustomerDetails();
    this.getAddresses();
    this.getPhones();
    this.getEmailAddresses();
    this.getContactPersons();
  }

  getCustomerDetails() {
    this.customerDetails = [];
    this.rest.getCustomerDetails(this.profileId, this.token).subscribe((data: {}) => {
      console.log(JSON.stringify(data));
      this.customerDetails = data[0];
    });
  }

  getAddresses() {
    this.addresses = [];
    this.rest.getAddresses(this.profileId, this.token).subscribe((data: {}) => {
      console.log(JSON.stringify(data));
      this.addresses = data[0];
    });
  }

  getPhones() {
    this.phones = [];
    this.rest.getPhones(this.profileId, this.token).subscribe((data: {}) => {
      console.log(JSON.stringify(data));
      this.phones = data[0];
    });
  }

  getEmailAddresses() {
    this.emailAddresses = [];
    this.rest.getEmailAddresses(this.profileId, this.token).subscribe((data: {}) => {
      this.emailAddresses = data;
    });
  }

  getContactPersons() {
    this.contactPersons = [];
    this.rest.getContactPersons(this.profileId, this.token).subscribe((data: {}) => {
      // console.log(data);
      console.log('The data' + JSON.stringify(data));
      this.contactPersons = data[0];
    });
  }

  sendBugReport() {
    this.rest.sendBugReport(this.customerDetails.Address, this.bugReport.title, this.bugReport.desc);
  }

}
