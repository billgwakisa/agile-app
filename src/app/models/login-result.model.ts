export class LoginResultModel {
  user: string;
  password: string;
  token: any;
  error: any;
  error_description: any;
}
