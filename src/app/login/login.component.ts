import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {UserServiceService} from '../user-service.service';
import {AuthApiService} from '../auth-api-service.service';
import {RestService} from '../rest.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  // title = 'Welcome to AGILIA';

  email = '';
  password = '';

  constructor(private api: AuthApiService, private user: UserServiceService, private router: Router, private rest: RestService) {
  }

  tryLogin() {
    if (this.email === '' || this.password === '') {
      alert('Please enter both email and password');
      return false;
    }
    this.api.login(
      this.email,
      this.password
    )
      .subscribe(
        r => {
          if (r['error'] === 'P2') {
            alert('Error: ' + r['error_description']);
          } else {
            console.log(r['access_token']);
            if (r['access_token'] !== '') {
              this.user.setToken(r['access_token'], r['agilia_user_id']);
              this.rest.tenantId = r['agilia_tenant_id'];
              // console.log(r.token.access_token);
              this.router.navigateByUrl('/home');
            }
          }
        },
        r => {
          console.log(`Message: ${r.message}`);
          console.log(r.error);
          alert(r);
        });

    // delete this and uncomment the top code
    // this.router.navigateByUrl('/home');
  }

}
