import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginResultModel} from './models/login-result.model';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class AuthApiService {

  constructor(private http: HttpClient) {

  }

  login(email: string, password: string): Observable<string> {
    return this.http.post<string>('http://localhost:5001/api/login', {
      username: email,
      password: password,
      grantType: environment.grant_type,
      client_id: environment.client_id,
      client_secret: environment.client_secret,
      isProfileLogin: environment.isProfileLogin,
      loginCode: environment.loginCode,
      resendLastCode: environment.resendLastCode,
      loginCookieCode: environment.loginCookieCode
    }, httpOptions);
  }
}
